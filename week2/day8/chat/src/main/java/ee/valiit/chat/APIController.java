package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin


public class APIController {


    @Autowired
    JdbcTemplate jdbcTemplate;

    //@RequestMapping
    //public APIController(){
    //    jdbcTemplate.execute("DROP TABLE IF EXISTS messages");
    //    jdbcTemplate.execute("CREATE TABLE messages (id INTEGER PRIMARY KEY, username text TEXT, message text TEXT, room text TEXT)");
    //}

    @GetMapping("/chat/{room}")
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        String sqlKask = "SELECT * FROM messages";
        ArrayList<ChatMessage> messages = (ArrayList)jdbcTemplate.query(sqlKask, (resultSet,rownum)-> {
            String username = resultSet.getString("username");
            String message = resultSet.getString("message");
            return new ChatMessage(username, message);
        });
        return messages;
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        String sqlKask = "INSERT INTO messages (username, message, room) VALUES ('" +
                msg.getUsername() + "', '" +
                msg.getMsg() + "', '" +
                room + "') ";
        jdbcTemplate.execute(sqlKask);
    }
}