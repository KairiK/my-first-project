package ee.valiit.chat;

public class ChatMessage {
    private int id;
    private String username;
    private String msg;
    private String room;

    public ChatMessage(){
    }

    public ChatMessage(String username, String msg){
        this.username = username;
        this.msg = msg;
    }


    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getMsg() {
        return msg;
    }

    public String getRoom() {
        return room;
    }
}
