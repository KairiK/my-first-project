console.log("Kes ei tööta see ei söö")



// 1. Ülesanne: Alla laadida APIst tekst.
var refreshMessages = async function() {
	console.log("refreshMessages läks käima") // kontrollin, kas töötab
	var APIurl = "http://138.197.191.73:8080/chat/general"
	// API aadress on string, salvestan muutujasse
	var request = await fetch(APIurl)
	// fetch teeb päringu serverisse (meie defineeritud aadress)
	var sonumid = await request.json()
	// json() käsk vormindab meile data mugavaks JSONiks

	//kuva serverist saadud info htmlis ehk lehel.
	document.querySelector('#jutt').innerHTML = ""
	while(sonumid.length > 0){ //kuniks sõnumeid on
		var sonum = sonumid.shift()  // enne oli pop(), mis lisas uued sõnumid üles, shift() lisab uued sõnumid üksteise alla
		// lisa HTMLi #jutt sisse sonum.message
		document.querySelector('#jutt').innerHTML += "<p>" + sonum.username + " ütleb: " + sonum.message + "</p>"
	}

	window.scrollTo(0, document.body.scrollHeight);
}
// uuenda sõnumeid iga sekund
setInterval(refreshMessages, 1000) // 1000 on sekund

document.querySelector('form').onsubmit = function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("submit käivitus")
	var kasutaja = document.querySelector("#nimi").value
	var message = document.querySelector("#msg").value
	document.querySelector('#msg').value = "" //teeb inputi tühjaks
	//document.querySelector('#jutt').innerHTML += "<br>" + kasutaja + " ütleb: " + message

	// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://localhost:8080/chat/general/new-message"
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({username: kasutaja, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'

		}


	})
}
