package ee.restovalija;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Tegevused {

    /*
    loeb kokku, mitu elementi restode ArrayListis on (mitu restorani on)
    tagastab suvalise arvu vahemikus 0 kuni (restode arv - 1), et saaks suvaliselt resto valida
     */
    public static int suvalineArv(ArrayList restod) {
        int num = restod.size();
        int randInt = (int)(Math.random() * num);
        return randInt;
    }

}
