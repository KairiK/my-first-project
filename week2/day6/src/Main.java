import java.util.Arrays;
import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {

        boolean first = Other.test(2);
        System.out.println(first);

        Other.test2("Tere", "tali");

        System.out.println(Other.addVat(10.0));

        System.out.println(Arrays.toString(Other.suva(1,2,true)));

        String gender = Other.deriveGender("49006131111");
        System.out.println(gender);

        Other.printHello();

        System.out.println(Other.deriveBirthYear("41806121111"));

        BigInteger ik = new BigInteger("49006131111");
        boolean verify = Other.validatePersonalCode(ik);
        System.out.println(verify);
    }
}
