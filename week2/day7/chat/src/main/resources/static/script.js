console.log("Kes ei tööta see ei söö")
var kumbChat = "general";
document.querySelector('#form2').onsubmit = function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("submit2 käivitus")
	kumbChat = document.querySelector('select').value
}

// 1. Ülesanne: Alla laadida APIst tekst.
var refreshMessages = async function() {
	console.log("refreshMessages läks käima") // kontrollin, kas töötab
	var APIurl = "http://localhost:8080/chat/"+kumbChat
	// API aadress on string, salvestan muutujasse
	var request = await fetch(APIurl)
	// fetch teeb päringu serverisse (meie defineeritud aadress)
	var sonumid = await request.json()
	console.log(sonumid);
	// json() käsk vormindab meile data mugavaks JSONiks

	//kuva serverist saadud info htmlis ehk lehel.
	document.querySelector('#jutt').innerHTML = ""
	//var sonumid = json.messages
	while(sonumid.length > 0){ //kuniks sõnumeid on
		var sonum = sonumid.shift()  // enne oli pop(), mis lisas uued sõnumid üles, shift() lisab uued sõnumid üksteise alla
		// lisa HTMLi #jutt sisse sonum.message
		//var img = new Image();
		document.querySelector('#jutt').innerHTML += "<p>" + '<img src="'+ sonum.avatar +'" width="50" height="60"/>' + sonum.username + " ütleb: " + sonum.message + "</p>"
		//document.querySelector('#jutt').innerHTML += `<p><img src='${sonum.avatar}' width="100">${sonum.username}:${sonum.message}</p>`
	}

	window.scrollTo(0, document.body.scrollHeight);
}
// uuenda sõnumeid iga sekund
setInterval(refreshMessages, 1000) // 1000 on sekund

document.querySelector('#form1').onsubmit = function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("submit käivitus")
	var username = document.querySelector("#nimi").value
	var message = document.querySelector("#msg").value
	var avatar = document.querySelector("#avatar").value
	document.querySelector('#msg').value = "" //teeb inputi tühjaks
	//document.querySelector('#jutt').innerHTML += "<br>" + kasutaja + " ütleb: " + message
    console.log(username, message)
	// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://localhost:8080/chat/"+kumbChat+"/new-message"
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({username: username, message: message, avatar: avatar}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'

		}
	})
}
