package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
@CrossOrigin

public class APIController{


    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping("/chat/{room}")
    ArrayList<ChatMessage> chat(@PathVariable String room) {

        try {
            String sqlKask = "SELECT * FROM messages WHERE room='"+room+"'";
            ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String username = resultSet.getString("username");
                String message = resultSet.getString("message");
                String avatar = resultSet.getString("avatar");

                return new ChatMessage(username, message, avatar);
            });
            return messages;
        } catch (DataAccessException err) {
            System.out.println("TABLE WAS NOT READY");
            return new ArrayList();
        }
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room){
        String sqlKask = "INSERT INTO messages (username, message, avatar, room) VALUES ('" +
                msg.getUsername() + "', '" +
                msg.getMessage() + "', '" +
                msg.getAvatar() + "', '" +
                room + "')";
        jdbcTemplate.execute(sqlKask);
    }

}

/* pikem versioon:
public class APIController {
    ChatRoom general = new ChatRoom("general");

    @GetMapping ("/chat/general")
    ChatRoom chat() {
        return general;
    }

    @PostMapping("/chat/general/new-message")
    void newMessage(@RequestBody ChatMessage msg){
        this.general.addMessage(msg);
    }


    ChatRoom random = new ChatRoom("random");
    @GetMapping ("chat/random")
    ChatRoom chatRand(){
        return random;
    }

    @PostMapping("chat/random/new-message")
    void newMessageRand(@RequestBody ChatMessage msg){
        this.random.addMessage(msg);
    }

}
*/
/*Seda saab ka lihtsustada:
public class APIController {
    HashMap<String, ChatRoom> rooms = new HashMap();

    public APIController(){
        rooms.put("general", new ChatRoom("general"));
        rooms.put("random", new ChatRoom("random"));
    }

    @GetMapping ("/chat/{room}")
    ChatRoom chat(@PathVariable String room){
        return rooms.get(room);
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage message, @PathVariable String room) {
    rooms.chatrooms.get(room).addMessage(msg);
    }

* */