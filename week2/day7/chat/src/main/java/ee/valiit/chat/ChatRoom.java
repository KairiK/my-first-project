package ee.valiit.chat;

import java.util.ArrayList;

public class ChatRoom {
    public String room;
    public ArrayList<ChatMessage> messages = new ArrayList();

    public ChatRoom(String room){
        this.room = room;
        messages.add(new ChatMessage("Juku", "Hei", "https://www.thesprucepets.com/thmb/dp-HvVRuM14H32PRu9FQmoDDMK0=/960x0/filters:no_upscale():max_bytes(150000):strip_icc():format(webp)/kitten-looking-at-camera-521981437-57d840213df78c583374be3b.jpg"));
    }

    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }
}
