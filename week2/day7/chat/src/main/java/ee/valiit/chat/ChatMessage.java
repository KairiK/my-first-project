package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {
    private int id;
    private String username;
    private String room;
    private String message;
    private String avatar; //pildile
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    //private Date date;

    public ChatMessage(String username, String message, String avatar){
        this.username = username;
        this.message = message;
        this.avatar = avatar;
        //this.date = new Date();
    }

    public ChatMessage(){

    }


    public String getUsername() {
        return Security.xssFix(username);
    }

    //public Date getDate() {
    //    return date;
   // }

   // public void setDate(Date date) {
    //    this.date = date;
   // }

    public String getMessage() {
        return Security.xssFix(message);
    }

    public String getAvatar() {
        return Security.xssFix(avatar);
    }

    public String getRoom() {
        return room;
    }

    public int getId() {
        return id;
    }
}
