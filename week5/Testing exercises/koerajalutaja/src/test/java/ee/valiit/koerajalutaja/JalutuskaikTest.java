package ee.valiit.koerajalutaja;

import org.junit.Assert;
import org.junit.Test;

public class JalutuskaikTest {
    Koer koer = new Koer("Pitsu", true, 3, 43);

    @Test
    public void saabRihmaKaela(){
        //tahame kinnitust, et koer hammustab
        Assert.assertEquals(true, koer.getHammustab());
        //tahame kinnitust saada, et Pitsu ei lase rihma kaela panna
        Assert.assertEquals("Koer hammustas sind! Ei saa rihma kaela panna.", Jalutuskaik.kasLasebRihmaPanna(koer));
    }

    @Test
    public void koerJäiRahule(){

        //koer jääb jalutuskäiguga rahule
        Assert.assertEquals(true, Jalutuskaik.kasKoerJäiRahule(koer, 45));
    }

    //kui teame, et peab testiga mingi error tulema:
    //@Test(expected - IllegalStateException.class)
    @Test
    public void kasMindiJalutama(){
        //given - koer
        //when
        //then
    }

    //Kui tahame endale sõnumit jätta mingi testi mittetoimimise kohta:
    // assertEquals("Wrong coffee type", Latte, coffee.getType())
    //see siis prindib errori puhul välja ka "Wrong coffee type", et
    //saaksime aru, milles probleem oli. See on Diagnostic message

    //teha test, millega kontrollida, et kui koer muutub vanemaks, siis jalutusaeg ka lüheneb
    @Test
    public void kasKoerVananedesTahabVahemJalutada(){
        int v6rdlus = (int) (koer.getJalutusAeg()-(koer.getJalutusAeg()*0.1));
        koer.vananeb();

        Assert.assertEquals("Jalutusaeg ei lühenenud", v6rdlus, koer.getJalutusAeg());
    }
}