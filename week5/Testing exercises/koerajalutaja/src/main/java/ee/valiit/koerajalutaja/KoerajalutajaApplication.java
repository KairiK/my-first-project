package ee.valiit.koerajalutaja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KoerajalutajaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KoerajalutajaApplication.class, args);
	}

}
