package ee.valiit.koerajalutaja;

public class Koer {
    private String nimi;
    private Boolean hammustab;
    private int vanus;
    private int jalutusAeg;

    public Koer(String nimi, Boolean hammustab, int vanus, int jalutusAeg){
        this.nimi = nimi;
        this.hammustab = hammustab;
        this.vanus = vanus;
        this.jalutusAeg = jalutusAeg;
    }

    public String getNimi() {
        return nimi;
    }

    public Boolean getHammustab() {
        return hammustab;
    }

    public void setHammustab(Boolean hammustab) {
        this.hammustab = hammustab;
    }

    public int getVanus() {
        return vanus;
    }

    public void setVanus(int vanus) {
        this.vanus = vanus;
    }

    public int getJalutusAeg() {
        return jalutusAeg;
    }

    public void setJalutusAeg(int jalutusAeg) {
        this.jalutusAeg = jalutusAeg;
    }

    public void vananeb() {
        this.vanus++;
        this.jalutusAeg = (int) (jalutusAeg - jalutusAeg * 0.1);
    }
}
