package ee.valiit.koerajalutaja;

public class Jalutuskaik {

    public static String kasLasebRihmaPanna(Koer koer){
        String reaktsioon = "";
        if(koer.getHammustab()){
            reaktsioon = "Koer hammustas sind! Ei saa rihma kaela panna.";
        } else {
            reaktsioon = "Said koerale rihma kaela.";
        }
        return reaktsioon;
    }

    public static Boolean kasMindiJalutama(Koer koer){
        if(Jalutuskaik.kasLasebRihmaPanna(koer).equals("Said koerale rihma kaela.")){
            return true;
        } else {
            return false;
        }
    }

    public static Boolean kasKoerJäiRahule(Koer koer, int aeg){
        if(aeg>=koer.getJalutusAeg()){
            return true;
        } else {
            return false;
        }
    }
}
