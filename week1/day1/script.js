console.log("Hello World!") //laseb väljundi Console'i

document.body.innerHTML += "<p>tere!</>" // = overridib olemasoleva HTMLi, 
//+= lisab olemasoleva lõppu

document.querySelector("ol li:nth-child(2").innerHTML = "Muudetud!"

//document = kogu dokument
// ol = ordered list HTMLi failis/veebilehel
//li = list item HTMLi failis/veebilehel
// "ol li:nth-child(2" - muudab teist

document.querySelector("form").onsubmit = function(e) {
	e.preventDefault()
	var nimeInput = document.querySelector("#nimi") //vt sama asja htmli failis
	console.log(nimeInput.value)
	if(nimeInput.value == "Kairi"){
		document.querySelector("#koht").innerHTML = "Tere, " + nimeInput.value
    } else {
    	document.querySelector("#koht").innerHTML = "A kus Kairi on?"
    }      
}
// {} vahel olev on plokk