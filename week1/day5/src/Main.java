import nadalavahetus.Pyhapaev;
import nadalavahetus.Reede;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hi!");

        //ÜL: loo kolm muutujat numbritega
        // moodusta lause
        //prindi

        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = aasta + ". aasta " + kuu + ". kuu " + paev + ". päeval sündis Krister.";
        System.out.println(lause);

        String parem = String.format("Mina sündisin aastal %d, %d. kuu %d. päeval.", aasta, kuu, paev);
        System.out.println(parem);

        // %d on täisarv
        // %f on komakohaga
        // %s on string


        // loe kokku mitu lampi on klassis ja pane see info hashmapi
        // loe kokku mitu akent on ja pane see info ka hashmapi
        //loe kokku mitu inimest on klassis ja pane see info ka hashmappi

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lambid", 11);
        klassiAsjad.put("inimesed", 21);
        // (võti, väärtus)

        System.out.println(klassiAsjad);
        // mingit järjekorda ei teki, tuleb suvalt.

        System.out.println(klassiAsjad.get("inimesed"));

        klassiAsjad.put(10, "tasapinnad");
        System.out.println(klassiAsjad);

        HashMap<String, Double> strDo = new HashMap();
        strDo.put("Mari", 5.0);
        strDo.put("Peedu", 4.5);


        //Ül switch
        int rongiNr = 50;
        String suund = null;
        switch (rongiNr) {
            case 50:
                suund = "Pärnu";
                break;
            case 55:
                suund = "Haapsalu";
                break;
            case 10:
                suund = "Vormsi";
                break;

        }
        System.out.println(suund);


        //For Each - veer üks tsükkel
        int[] mingidNr = new int[]{8, 6, 54, 365, 5554, 23564};
        for (int i = 0; i < mingidNr.length; i++) {
            System.out.println(mingidNr[i]);
        }

        System.out.println(".......");

        for (int nr : mingidNr) {
            System.out.println(nr);
        }

        // Ül: Õpilane saab töö eest punkte 0-100
        //kui alla 50, kukub läbi, muidu hinne = punktid/20
        // 100p - 5
        // 80p - 4
        // 67p - 3
        // 50p - 2

        int punkte = 99;
        if (punkte > 100 || punkte < 0){
            throw new Error();
        }
        switch((int) Math.round(punkte/20.0)){
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("eee... õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi");
        }

        Reede.koju();

        Pyhapaev pyha = new Pyhapaev();
        pyha.maga();
    }
}
