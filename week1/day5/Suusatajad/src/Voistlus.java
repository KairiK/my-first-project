import java.util.ArrayList;

public class Voistlus {
    ArrayList<Suusataja>voistlejad;
    int koguDistants;
    public Voistlus(){
        System.out.println("Start");
        voistlejad = new ArrayList();
        koguDistants = 20;
        for (int i = 0; i < 10; i++) {
            voistlejad.add(new Suusataja(i));
        }

        aeg();
    }
    public void aeg() {
        for (Suusataja s: voistlejad) {
            s.suusata();
            s.kasKatkestab();
            System.out.println(s);

            boolean lopetanud = s.kasOnLopetanud(koguDistants);
            if (lopetanud) {
                System.out.println("Võitja on: " + s);
                return;
            }
        }
        System.out.println("-------------------");


        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aeg();
    }

}
