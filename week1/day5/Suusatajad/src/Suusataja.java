public class Suusataja {

    int stardiNr;
    double kiirus;
    double labitudDistants;
    double dopinguKordaja; // kodune ülesanne
    boolean katkestas;


    public Suusataja(int i) {
        stardiNr = i;
        kiirus = Math.random() * 19 + 5; // 20 km/t
        labitudDistants = 0;
        dopinguKordaja = Math.round(Math.random() * 10);
        katkestas = false;
    }

    public void suusata() {
        if (katkestas) {
            labitudDistants += 0;
        } else {
            if (dopinguKordaja == 2.0) {
                labitudDistants += (kiirus / 3600 * 4);
            } else {
                labitudDistants += kiirus / (3600);
            }
        }
    }

    public String toString() {
        int dist = (int) (labitudDistants);
        String nool = stardiNr + ": ";
        for (int i = 0; i < dist; i++) {
            nool += "=";
        }
        nool += ">";
        if (katkestas) {
            nool = "Võistleja " + stardiNr + " kakestas.";
        }
        return nool;
    }

    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }

    public void kasKatkestab() {
        double num = Math.round(Math.random() * 10000);
        if (num == 5.0) {
            katkestas = true;
        }
    }
}
