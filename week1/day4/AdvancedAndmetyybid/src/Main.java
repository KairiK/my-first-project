import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //Keskseks teemaks on array ehk massiiv
        int[] massiiv = new int[6];
        ArrayList list = new ArrayList();


        //Ül: prindi välja massiiv

        System.out.println(massiiv);
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        //Ül: muuda kolmandal positsioonil olev number viieks
        massiiv[2] = 5;
        String massiivStr2 = Arrays.toString(massiiv);
        System.out.println(massiivStr2);


        massiiv[5] = 7;
        System.out.println(massiiv[5]);

        int viimane = massiiv[massiiv.length - 1];
        System.out.println(viimane);

        // loo massiiv, kus kõik 8 nrit kohe määratud
        int[] massiiv2 = new int[] {1,2,3,4,5,6,7,8};
        System.out.println(Arrays.toString(massiiv2));

        //prindi ükshaaval välja kõik väärtused
        int index = 0;
        while(index < massiiv2.length) {
            System.out.println(massiiv2[index]);
            index++;
        }

        //sama tsükkel for-iga
        for (int i = 0; i < massiiv2.length; i++) {
            System.out.println(massiiv2[i]);
        }

        // loo stringide massiiv, mis on algul tühi
        //aga siis lisad veel keskele mingi sõne

        String[] soned = new String[9];
        System.out.println(Arrays.toString(soned));
        soned[3] = "Hello world!";
        System.out.println(Arrays.toString(soned));

        //veel ülesandeid


        int[] megaMassiiv = new int[100];
        for (int j = 0; j < 100; j++) {
            megaMassiiv[j] = j;

        }
        System.out.println(Arrays.toString(megaMassiiv));

        int paaris = 0;
        for (int i = 0; i < megaMassiiv.length; i++) {
            if(megaMassiiv[i] % 2 == 0) {
                paaris++;
            }
        }
        System.out.println(paaris);

        ArrayList kodune = new ArrayList();
        for (int i = 0; i < 543; i++) {
            double nr = Math.round(Math.random() * 10);
            kodune.add(nr);
        }
        System.out.println(kodune);
        for (int i = 0; i < kodune.size(); i++) {
            double num = (double) kodune.get(i);
            kodune.set(i, (num*5));
        }
        System.out.println(kodune);
    }
}
