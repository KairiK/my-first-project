console.log("tere!")

var sisend = 8
var tulemus

if(sisend < 7){
	tulemus = sisend * 2
} else if(sisend > 7) {
	tulemus = sisend / 2
} else {
	tulemus = sisend
}

console.log("Tulemus on: " + tulemus)

var str1 = "banaan"
var str2 = "apelsin"

if(str1 == str2){ //kusjuures js-is saab ka võrrelda int ja str omavahel; 
	console.log(str1)  //kui seda rangelt ei taha, siis tuleb kasutada ===
} else {
	console.log(str1+ " " + str2)
}


var linnad = ["Tallinn", "Tartu", "Valga", "Rakvere", "Pärnu"]
var uus = []

while(linnad.length > 0){
	var linn = linnad.pop()
	var uusLinn = linn + " linn"
	uus.push(uusLinn)
}
console.log(uus)

var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while(nimed.length > 0){
	var nimi = nimed.pop()
	if(nimi.endsWith("a")){ // alternatiiv nimi.endsWith("a")
		tydrukuteNimed.push(nimi)
	} else {
		poisteNimed.push(nimi)
	}
}
console.log(poisteNimed, tydrukuteNimed)

/* FUNKTSIOONID*/

var eristaja = function(nimi){
	if(nimi.endsWith("a")){ 
		return "tüdruk"
	} else {
		return "poiss"
	}	
}
var nimieristaja = "Peeter"
var sugu = eristaja(nimieristaja)
console.log(sugu)


var kasOnNumber = function(sisend){
	if(isNaN(sisend)) {
		return "Ei"
	} else {
		return "Jah"
	}
}


console.log(kasOnNumber(4))
console.log(kasOnNumber("mingi sõne"))
console.log(kasOnNumber(23536))
console.log(kasOnNumber(6.43))
console.log(kasOnNumber(null))
console.log(kasOnNumber([2, 3, 4]))

var summa = function(num1, num2) {
	var vastus = num1 + num2
	return vastus
}
console.log(summa(4, 2))
console.log(summa(10, 23))

console.log("---------")

var inimesed = {
	"Kaarel": 34,
	"Margarita": 10, 
	"Suksu": [5, 3, 5, 2],
	"Krister": {
		vanus: 30,
		sugu: true
	}
}

console.log(inimesed["Kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Krister.vanus)
console.log(inimesed.Suksu[1])