public class Main {
    public static void main(String[] args) {
        Koer pontu = new Koer();
        pontu.lausu();
        pontu.maga();

        Kass nurr = new Kass();
        nurr.lausu();
        nurr.maga();

        pontu.ajaPeremeesYles();
        nurr.ajaPeremeesYles();

        Kilpkonn kilbik = new Kilpkonn();
        kilbik.lausu();

        System.out.println("Kui kassil paluda saba kinni püüda: ");
        System.out.println(nurr.getSaba());
    }
}
