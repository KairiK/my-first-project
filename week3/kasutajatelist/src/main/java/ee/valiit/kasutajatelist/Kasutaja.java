package ee.valiit.kasutajatelist;

public class Kasutaja {
    private String nimi;
    private int vanus;

    public Kasutaja(){

    }

    public Kasutaja(String nimi, int vanus){
        this.nimi = nimi;
        this.vanus = vanus;
    }

    public String getNimi() {
        return nimi;
    }

    public int getVanus() {
        return vanus;
    }
}
