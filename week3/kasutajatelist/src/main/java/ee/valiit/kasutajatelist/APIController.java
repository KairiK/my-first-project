package ee.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

//@ kohta vaata Spring documentationist
@RestController
@CrossOrigin
public class APIController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping ("/list")
    public void handleNewListItem(@RequestBody Kasutaja kasutaja){
        //requestBody ütleb, et midagi peab vastu võtma, tuleb päringust endast
        //tuleb luua uues klass Kasutaja, et päringuid vastu võtta (siiani ühtegi uut klassi teinud polnud)
        System.out.println("handleNewListItem");
        System.out.println("Kasutaja nimi: " + kasutaja.getNimi());

        String sqlKask = "INSERT INTO kasutajad (nimi, vanus) VALUES ('"+ kasutaja.getNimi() +"',"+ kasutaja.getVanus() +")"; //vanuse ümber pole " " vaja, sest vanus on number
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus");
    }

    @GetMapping ("/list")
    ArrayList<Kasutaja> returnListItem(){
        System.out.println("returnListItem");
        String sqlKask = "SELECT * FROM kasutajad";
        ArrayList<Kasutaja> kasutajad = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum)->{
            String nimi = resultSet.getString("kasutaja");
            int vanus = resultSet.getInt("vanus");

            return new Kasutaja(nimi, vanus);
        });
        return kasutajad;
    }
}
