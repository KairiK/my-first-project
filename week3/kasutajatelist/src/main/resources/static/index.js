document.querySelector('#koht').innerHTML = ""

var loadList = async function(){
    console.log("loadList läks tööle")
    var response = await fetch("/list")
    var body = await response.json()
    console.log(body)
    var isikud = ""
    for(element of body){
        var nimi = body.nimi
        var vanus = body.vanus
        isikud += "Nimi: "+nimi+", vanus: "+vanus+"<br>"
    }
    document.querySelector('#koht').innerHTML = isikud
}

var url = "http://localhost:8080/list"
document.querySelector("#form1").onsubmit = function(e){
    console.log("onsubmit toimib")
    e.preventDefault() // siis ei refreshi kohe automaatselt

    //määrame formi põhjal väärtused
    var nimi = document.querySelector("#nimi").value
    var vanus = document.querySelector("#vanus").value

    fetch(url, {
        method: "POST",
        body: JSON.stringify({nimi, vanus}),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    })
    loadList()
}


