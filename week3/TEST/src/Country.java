import java.util.List;

public class Country {
    /*
    Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName,
    setName ja list riigis enim kõneldavate keeltega.
    Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
    mis sisaldaks endas kõiki parameetreid väljaprindituna.
    Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info
    välja .toString() meetodi abil.
     */
    //defineerin muutujad
    private String name;
    private int population;
    private List<String> languages;

    //loon konstruktori
    public Country(String name, int population, List<String> languages){
        this.name = name;
        this.population = population;
        this.languages = languages;
    }

    //overridin toString() meetodi
    public String toString(){
        //muudan keelte Listi nii, et välja printides oleks ilusam vaadata.
        String languagesString = "";
        for(String language : languages){
             languagesString += language + ", ";
        }
        languagesString = languagesString.substring(0, languagesString.length()-2);

        //tagastan muutujad soovitud vormis
        return "Name: "+name+", Population: "+population+", Most spoken languages: "+languagesString;
    }

    //loon getterid ja setterid
    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
