import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        /* Ülesanne 1
        Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist väärtust vähemalt 11 kohta peale koma.
        Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.
         */
        System.out.println("Ülesanne 1:");
        //deklaleerin Pi, rohkem kui 11 kohta peale koma
        double pi = Math.PI;
        //väljastan pi korrutatud kahega
        System.out.println(pi*2);


        /* Ülesanne 2
        Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
        Meetod tagastab tõeväärtuse vastavalt sellele, kas kaks sisendparametrit on omavahel võrdsed või mitte.
        Meetodi nime võid ise välja mõelda.
        Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.
         */
        System.out.println("Ülesanne 2:");
        //loon muutuja, milles hoida oma meetodi väärtust
        boolean kasOnVordne = isEqual(5, 2);
        //väljastan selle muutuja
        System.out.println(kasOnVordne);


        /* Ülesanne 3
        Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab täisarvude massiivi.
        Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
        Meetodi nime võid ise välja mõelda.
        Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid) standardväljundisse.
         */
        System.out.println("Ülesanne 3:");
        //loon stringide massiivi
        String[] animals = {"dog", "horse", "hamster", "hippopotamus"};
        //loon täisarvude massiivi muutuja, millesse salvestan meetodi tulemuse
        int[] animalLength = strLengthCounter(animals);
        //väljastan selle muutuja
        System.out.println(Arrays.toString(animalLength));


        /* Ülesanne 4
        Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int) vahemikus 1 - 2018 ja
        tagastab täisarvu (byte) vahemikus 1 - 21 vastavalt sellele, mitmendasse sajandisse antud aasta kuulub.
        Meetodi nime võid ise välja mõelda. Kui funktsioonile antakse ette parameeter, mis on kas suurem, kui 2018
        või väiksem, kui 1, tuleb tagastada väärtus -1;
        Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega: 0, 1, 128, 598, 1624, 1827,
        1996, 2017. Prindi need väärtused standardväljundisse.
         */
        System.out.println("Ülesanne 4:");
        //kutsun meetodi välja ette antud sisenditega ja väljastan tulemused.
        System.out.println("0: "+returnCentury(0));
        System.out.println("1: "+returnCentury(1));
        System.out.println("128: "+returnCentury(128));
        System.out.println("598: "+returnCentury(598));
        System.out.println("1624: "+returnCentury(1624));
        System.out.println("1827: "+returnCentury(1827));
        System.out.println("1996: "+returnCentury(1996));
        System.out.println("2017: "+returnCentury(2017));


        /* Ülesanne 5:
        Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja list
        riigis enim kõneldavate keeltega.
        Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti,
        mis sisaldaks endas kõiki parameetreid väljaprindituna.
        Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info
        välja .toString() meetodi abil.
         */
        System.out.println("Ülesanne 5:"); //.
        //loon klassist objekti
        Country India = new Country("India", 1364343492, Arrays.asList("Assamese", "Bengali", "Bodo",
                "Dogri", "Guajarati", "Hindi", "Kannada", "Kashmiri", "Konkani", "Maithili", "Malayalam", "Marathi",
                "Meitei", "Nepali", "Oriya", "Eastern Panjabi", "Sanskrit", "Santali", "Sindhi", "Tamil", "Telugu",
                "Urdu"));
        //väljastan selle
        System.out.println(India.toString());
    }

    //Ülesande 2 meetod
    public static boolean isEqual(int a, int b){
        //tagastab true, kui a ja b on võrdsed.
        if(a==b){
            return true;
        }else{
            return false;
        }
    }

    //Ülesande 3 meetod
    public static int[] strLengthCounter(String[] a){
        //loon massiivi muutuja, millesse täisarve salvestada
        int[] strLength = new int[a.length];
        //loen ükshaaval kokku iga sõne tähtede arvu ja salvestan uude massiivi
        for (int i = 0; i < a.length; i++) {
            String str = a[i];
            strLength[i] = str.length();
        }
        return strLength;
    }

    //Ülesande 4 meetod
    public static byte returnCentury(int a){
        //loon tagastatava muutuja
        byte century = 0;
        //vastavalt etteantud juhistele määran, mida peaks tagastama
        if(a<1||a>2018){
            return -1;
        }else{
            if(a<100) {
                century = 1;
            } else if(a<200){
                century = 2;
            } else if(a<300){
                century = 3;
            } else if(a<400){
                century = 4;
            } else if(a<500){
                century = 5;
            } else if(a<600){
                century = 6;
            } else if(a<700){
                century = 7;
            } else if(a<800){
                century = 8;
            }else if(a<900){
                century = 9;
            }else if(a<1000){
                century = 10;
            }else if(a<1100){
                century = 11;
            }else if(a<1200){
                century = 12;
            }else if(a<1300){
                century = 13;
            }else if(a<1400){
                century = 14;
            }else if(a<1500){
                century = 15;
            }else if(a<1600){
                century = 16;
            }else if(a<1700){
                century = 17;
            }else if(a<1800){
                century = 18;
            }else if(a<1900){
                century = 19;
            }else if(a<2000){
                century = 20;
            }else if(a<2100){
                century = 21;
            }
        }
        return century;
    }
}
