public class MyClass {
    private static final int num = 100;

    public void add(int num) {
        MyClass.num = num * 10;
    }
    public int getNum() {
        return num;
    }

    public static void main(String args[]) {
        MyClass mc = new MyClass();
        mc.add(5);
        System.out.println(mc.getNum());
    }
}