import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result {

    /*
     * Complete the 'linker' function below.
     *
     * The function is expected to return a list of matches.
     * The function accepts following parameters:
     *  1. List of bankstatement
     *  2. List of transfers
     *
     * SEE EXMAPLE above
     */

    public static List<List<String>> linker(List<List<String>> bankStatment, List<List<String>> transfers) {
        List<List<String>> allMatches = new ArrayList<List<String>>();
        try {
            for (int i = 0; i < bankStatment.size(); i++) {
                List<String> ifMatch = new ArrayList<String>();
                int matches = 0;
                String confidence = "";
                if (bankStatment.get(i).get(1).toLowerCase().equals(transfers.get(i).get(1).toLowerCase())) {
                    matches++; //for first name
                }
                if (bankStatment.get(i).get(2).toLowerCase().equals(transfers.get(i).get(2).toLowerCase())) {
                    matches++; //for last name
                }
                if (bankStatment.get(i).get(4).toLowerCase().equals(transfers.get(i).get(3).toLowerCase())) {
                    matches++; //for amount
                }
                if (bankStatment.get(i).get(5).toLowerCase().equals(transfers.get(i).get(4).toLowerCase())) {
                    matches++; //for currency
                }
                if (bankStatment.get(i).get(6).toLowerCase().equals(transfers.get(i).get(5).toLowerCase())) {
                    matches++; //for comment
                }

                if (matches == 5) {
                    ifMatch.add(bankStatment.get(i).get(0));
                    ifMatch.add(transfers.get(i).get(0));
                    ifMatch.add("match");
                } else if (matches == 4) {
                    ifMatch.add(bankStatment.get(i).get(0));
                    ifMatch.add(transfers.get(i).get(0));
                    ifMatch.add("high");
                } else if (matches == 3) {
                    ifMatch.add(bankStatment.get(i).get(0));
                    ifMatch.add(transfers.get(i).get(0));
                    ifMatch.add("proposed");
                } else {
                    ifMatch.add(bankStatment.get(i).get(0));
                    ifMatch.add(transfers.get(i).get(0));
                    ifMatch.add("none");
                }
                allMatches.add(ifMatch);

            }
        }catch (Exception e){
            System.out.println("Some problem");
        }
        return allMatches;
    }

}