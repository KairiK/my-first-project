import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> statement1 = new ArrayList<>();
        statement1.add("a1TVPS+joiQ=");
        statement1.add("Beebe");
        statement1.add("Aarika");
        statement1.add("FR19-2312-9002-35GW-20YN-STX5-095");
        statement1.add("282.0");
        statement1.add("CNY");
        statement1.add("P663");
        List<String> statement2 = new ArrayList<>();
        statement2.add("zOvfqFxTWqQ=");
        statement2.add("Fancy");
        statement2.add("Mischa");
        statement2.add("DO76-63II-8715-5143-3546-5715-9731");
        statement2.add("331.0");
        statement2.add("MYR");
        statement2.add("P96047");
        List<List<String>> statements = new ArrayList<>();
        statements.add(statement1);
        statements.add(statement2);

        List<String> transfer1 = new ArrayList<>();
        transfer1.add("WlQ5WsLH+fU=");
        transfer1.add("Beebe");
        transfer1.add("Aarika");
        transfer1.add("282.0");
        transfer1.add("CNY");
        transfer1.add("P663");
        transfer1.add("37");
        List<String> transfer2 = new ArrayList<>();
        transfer2.add("qWNmZankudw=");
        transfer2.add("Fancy");
        transfer2.add("Mischa");
        transfer2.add("331.0");
        transfer2.add("MYR");
        transfer2.add("P96047");
        transfer2.add("676");
        List<List<String>> transfers = new ArrayList<>();
        transfers.add(transfer1);
        transfers.add(transfer2);
        System.out.println();
        System.out.println(Result.linker(statements, transfers));
    }
}
